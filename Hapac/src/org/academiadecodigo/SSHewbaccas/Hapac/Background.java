package org.academiadecodigo.SSHewbaccas.Hapac;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Background {

    public static final int PADDING = 10;
    public Picture picture;

    public Background() {
        picture = new Picture(PADDING, PADDING, "BackgroundFixed.png");
        picture.draw();
    }

    public int getWidth() {
        return picture.getWidth();
    }
    public int getHeight() {
        return picture.getHeight();
    }


}
