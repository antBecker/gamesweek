package org.academiadecodigo.SSHewbaccas.Hapac;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class Movement implements KeyboardHandler {

    private Keyboard keyboard;
    private Player player;

    public Movement(Player player) {
        this.player = player;
        keyboard = new Keyboard(this);
    }

    public void init() {
        KeyboardEvent dPress = new KeyboardEvent();
        dPress.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        dPress.setKey(KeyboardEvent.KEY_D);

        KeyboardEvent aPress = new KeyboardEvent();
        aPress.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        aPress.setKey(KeyboardEvent.KEY_A);

        KeyboardEvent spacePress = new KeyboardEvent();
        spacePress.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        spacePress.setKey(KeyboardEvent.KEY_SPACE);

        KeyboardEvent kPress = new KeyboardEvent();
        kPress.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        kPress.setKey(KeyboardEvent.KEY_K);

        KeyboardEvent qPressed = new KeyboardEvent();
        qPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        qPressed.setKey(KeyboardEvent.KEY_Q);

        KeyboardEvent dRelease = new KeyboardEvent();
        dRelease.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        dRelease.setKey(KeyboardEvent.KEY_D);

        KeyboardEvent aRelease = new KeyboardEvent();
        aRelease.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        aRelease.setKey(KeyboardEvent.KEY_A);

        KeyboardEvent kRelease = new KeyboardEvent();
        kRelease.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        kRelease.setKey(KeyboardEvent.KEY_K);


        keyboard.addEventListener(dPress);
        keyboard.addEventListener(aPress);
        keyboard.addEventListener(spacePress);
        keyboard.addEventListener(kPress);
        keyboard.addEventListener(kRelease);
        keyboard.addEventListener(qPressed);
        keyboard.addEventListener(dRelease);
        keyboard.addEventListener(aRelease);
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_D) {
            player.setMovingLeft(false);
            player.setMovingRight(true);
            player.deleteRun();
            player.moveRight();
        }

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_A) {
            player.setMovingLeft(true);
            player.setMovingRight(false);
            player.deleteLeftRun();
            player.moveLeft();
        }

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_SPACE) {
            player.setJumping(true);
            player.jump();
        }

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_K) {

            if(player.getLastDirection() == 0) {
                player.playerAttackRight();
            }

            if (player.getLastDirection() == 1) {
                player.playerAttackLeft();
            }

            /*System.out.println("Press K");*/
        }

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_Q) {
            System.exit(1);
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_D) {
          player.setMovingRight(false);
        }

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_A) {
            player.setMovingLeft(false);
        }

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_K) {

            if(player.playerAttackToRight) {
                player.deleteAttackRight();
            }

            if(player.playerAttackToLeft){
                player.deleteAttackLeft();
            }
        }
    }
}
