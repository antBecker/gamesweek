package org.academiadecodigo.SSHewbaccas.Hapac.GridStuff;

import org.academiadecodigo.SSHewbaccas.Hapac.Interfaces.Grid;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class SimpleGfxGrid implements Grid {
    private int x;
    private int y;
    Rectangle playArea;
    public static final int PADDING = 10;
    private Picture picture;
    private Rectangle rec;


    public void init(){
        picture = new Picture(PADDING,PADDING,"BackgroundFixed.png");
        rec = new Rectangle(PADDING, PADDING, picture.getWidth(), picture.getHeight());
        /*System.out.println(picture.getWidth());
        System.out.println(picture.getHeight());*/
        rec.draw();
        picture.draw();
    }

    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }



    public void  setX(int x){
        this.x = x;
    }

    public void setY(int y){
        this.y = y;
    }

}
