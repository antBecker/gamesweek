package org.academiadecodigo.SSHewbaccas.Hapac;

import org.academiadecodigo.SSHewbaccas.Hapac.GridStuff.SimpleGfxGrid;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Player {

    private SimpleGfxGrid grid;

    private int playerHealthBar = 100;

    private int x;
    private int y;

    private int playerDamage = 15;

    //Chico & Alex
    public boolean isJumping = false;

    /*public boolean pressed = false;*/
    public boolean isMovingRight = false;
    public boolean isMovingLeft = false;

    //Last direction. 0: right|| 1: left.
    private int lastDirection = 0;

    public int getLastDirection() {
        return lastDirection;
    }

    public boolean playerLookingRight = false;
    public boolean playerLookingLeft = false;

    public boolean playerAttackToRight = false;
    public boolean playerAttackToLeft = false;

    //Ground at 695
    //ATTACK
    private Picture att1Right;
    private Picture att2Right;
    private Picture att3Right;
    private Picture att4Right;
    private Picture att5Right;

    private Picture att1Left;
    private Picture att2Left;
    private Picture att3Left;
    private Picture att4Left;
    private Picture att5Left;

    //IDLE
    private Picture p0;
    private Picture p1;
    private Picture p2;
    private Picture p3;

    //JUMP
    private Picture j0;
    private Picture j1;
    private Picture j2;
    private Picture j3;
    //RUN
    private Picture r0;
    private Picture r1;
    private Picture r2;
    private Picture r3;

    //RUN LEFT
    private Picture rL0;
    private Picture rL1;
    private Picture rL2;
    private Picture rL3;

    public Player(int x, int y, SimpleGfxGrid grid) {
        this.x = x;
        this.y = y;
        this.grid = grid;
        idlePlayer(counter);

    }

    public int getPlayerDamage() {
        return playerDamage;
    }

    public int getPlayerHealthBar() {
        return playerHealthBar;
    }

    public void setPlayerHealthBar(int attackPower) {
        this.playerHealthBar -= attackPower;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setMovingRight(boolean movingRight) {
        isMovingRight = movingRight;
    }

    public void setMovingLeft(boolean movingLeft) {
        isMovingLeft = movingLeft;
    }

    int counter = 0;
    public int getCounter(int counter) {
        return this.counter = counter;
    }

    public void moveRight() {

        playerLookingRight = true;
        playerLookingLeft = false;
        lastDirection = 0;
        deleteIdle();

        switch (counter) {
            case 1:
                r0 = new Picture(x, y, "Player/Run/" + counter + ".png");
                r0.grow(10, 10);
                r0.draw();
                if (r3 != null) {r3.delete();}
                break;
            case 2:
                r1 = new Picture(x, y, "Player/Run/" + counter + ".png");
                r1.grow(10, 10);
                r1.draw();
                if (r0 != null) {r0.delete();}
                break;
            case 3:
                r2 = new Picture(x, y, "Player/Run/" + counter + ".png");
                r2.grow(10, 10);
                r2.draw();
                if (r1 != null) {r1.delete();}
                break;
            default:
                r3 = new Picture(x, y, "Player/Run/" + counter + ".png");
                r3.grow(10, 10);
                r3.draw();
                if (r2 != null) {r2.delete();}
                break;
        }
            /*System.out.println("player: " + x);*/

            if (x < 870) {x += 8;}
    }

    public void moveLeft() {

        playerLookingRight = false;
        playerLookingLeft = true;
        lastDirection = 1;

        deleteIdle();
        switch (counter) {
            case 1:
                rL0 = new Picture(x, y, "Player/RunLeft/" + counter + ".png");
                rL0.grow(10, 10);
                rL0.draw();
                if (rL3 != null) {rL3.delete();}
                break;
            case 2:
                rL1 = new Picture(x, y, "Player/RunLeft/" + counter + ".png");
                rL1.grow(10, 10);
                rL1.draw();
                if (rL0 != null) {rL0.delete();}
                break;
            case 3:
                rL2 = new Picture(x, y, "Player/RunLeft/" + counter + ".png");
                rL2.grow(10, 10);
                rL2.draw();
                if (rL1 != null) {rL1.delete();}
                break;
            default:
                rL3 = new Picture(x, y, "Player/RunLeft/" + counter + ".png");
                rL3.grow(10, 10);
                rL3.draw();
                if (rL2 != null) {rL2.delete();}
                break;
        }
        if (x > SimpleGfxGrid.PADDING) {x -= 8;}
    }

    public void isJumping() {
        isJumping = false;
    }

    public void setJumping(boolean jumping) {
        isJumping = jumping;
    }

    public void jump() {

        if (isJumping) {
            Picture chosenPicture = null;
            deleteIdle();
            deleteLeftRun();
            deleteRun();
            if (chosenPicture == j1) {
                j1 = new Picture(x, y, "Player/Jump/" + counter + ".png");
                j1.grow(10, 10);
                j1.draw();
                chosenPicture = j2;
                if (j0 != null) {
                    j0.delete();
                }
            } else if (chosenPicture == j2) {
                j2 = new Picture(x, y, "Player/Jump/" + counter + ".png");
                j2.grow(10, 10);
                j2.draw();
                chosenPicture = j3;
                if (j1 != null) {
                    j1.delete();
                }
            } else if (chosenPicture == j3) {
                j3 = new Picture(x, y, "Player/Jump/" + counter + ".png");
                j3.grow(10, 10);
                j3.draw();
                chosenPicture = j0;
                isJumping = false;
                if (j2 != null) {
                    j2.delete();
                }
            } else if ((chosenPicture == null) || (chosenPicture == j0)) {
                j0 = new Picture(x, y, "Player/Jump/" + counter + ".png");
                j0.grow(10, 10);
                j0.draw();
                chosenPicture = j1;
                if (j3 != null) {
                    j3.delete();
                }
            }

            if (y < 685) {
                int thisCounter = 1;
                int jumpSpeed = -40 / thisCounter;
                y -= jumpSpeed;
                thisCounter += 1;

            }
        }
    }

    public void playerAttackRight() {

        playerAttackToRight = true;
        playerAttackToLeft = false;


        att1Right = new Picture(x, 695, "Player/Attack1/adventurer-attack1-00.png");
        att1Right.grow(10, 10);
        att1Right.draw();
        att2Right = new Picture(x, 695, "Player/Attack1/adventurer-attack1-01.png");
        att2Right.grow(10, 10);
        att2Right.draw();
        att3Right = new Picture(x, 695, "Player/Attack1/adventurer-attack1-02.png");
        att3Right.grow(10, 10);
        att3Right.draw();
        att4Right = new Picture(x, 695, "Player/Attack1/adventurer-attack1-03.png");
        att4Right.grow(10, 10);
        att4Right.draw();
        att5Right = new Picture(x, 695, "Player/Attack1/adventurer-attack1-04.png");
        att5Right.grow(10, 10);
        att5Right.draw();
    }

    public void playerAttackLeft(){

        playerAttackToRight = false;
        playerAttackToLeft = true;



            att1Left = new Picture(x, 695, "Player/Attack1/flipped-00.png");
            att1Left.grow(10, 10);
            att1Left.draw();
            att2Left = new Picture(x, 695, "Player/Attack1/flipped-01.png");
            att2Left.grow(10, 10);
            att2Left.draw();
            att3Left = new Picture(x, 695, "Player/Attack1/flipped-02.png");
            att3Left.grow(10, 10);
            att3Left.draw();
            att4Left = new Picture(x, 695, "Player/Attack1/flipped-03.png");
            att4Left.grow(10, 10);
            att4Left.draw();
            att5Left = new Picture(x, 695, "Player/Attack1/flipped-04.png");
            att5Left.grow(10, 10);
            att5Left.draw();

    }

    public void deleteAttackRight() {
        att1Right.delete();
        att2Right.delete();
        att3Right.delete();
        att4Right.delete();
        att5Right.delete();
    }

    public void deleteAttackLeft(){
        att1Left.delete();
        att2Left.delete();
        att3Left.delete();
        att4Left.delete();
        att5Left.delete();
    }

    private void deleteIdle() {
        if (p0 != null) {p0.delete();}
        if (p1 != null) {p1.delete();}
        if (p2 != null) {p2.delete();}
        if (p3 != null) {p3.delete();}
    }

    public void deleteRun() {
        if (r0 != null) {r0.delete();}
        if (r1 != null) {r1.delete();}
        if (r2 != null) {r2.delete();}
        if (r3 != null) {r3.delete();}
    }

    public void deleteLeftRun() {
        if (rL0 != null) {rL0.delete();}
        if (rL1 != null) {rL1.delete();}
        if (rL2 != null) {rL2.delete();}
        if (rL3 != null) {rL3.delete();}
    }

    public void idlePlayer(int counter){

        if (!isMovingRight && !isMovingLeft && playerLookingRight) {

            switch (counter) {

                default:
                    p0 = new Picture(x, y, "Player/Idle/" + counter + ".png");
                    p0.grow(10, 10);
                    p0.draw();
                    if (p3 != null) {p3.delete();}
                    break;
                case 2:
                    p1 = new Picture(x, y, "Player/Idle/" + counter + ".png");
                    p1.grow(10, 10);
                    p1.draw();
                    if (p0 != null) {p0.delete();}
                    break;
                case 3:
                    p2 = new Picture(x, y, "Player/Idle/" + counter + ".png");
                    p2.grow(10, 10);
                    p2.draw();
                    if (p1 != null) {p1.delete();}
                    break;
                case 4:
                    p3 = new Picture(x, y, "Player/Idle/" + counter + ".png");
                    p3.grow(10, 10);
                    p3.draw();
                    if (p2 != null) {p2.delete();}
                    break;
            }
            deleteRun();
            deleteLeftRun();

        }
        if (!isMovingRight && !isMovingLeft && playerLookingLeft) {

            switch (counter) {

                default:
                    p0 = new Picture(x, y, "Player/Idle/0" + counter + ".png");
                    p0.grow(10, 10);
                    p0.draw();
                    if (p3 != null) {p3.delete();}
                    break;
                case 2:
                    p1 = new Picture(x, y, "Player/Idle/0" + counter + ".png");
                    p1.grow(10, 10);
                    p1.draw();
                    if (p0 != null) {p0.delete();}
                    break;
                case 3:
                    p2 = new Picture(x, y, "Player/Idle/0" + counter + ".png");
                    p2.grow(10, 10);
                    p2.draw();
                    if (p1 != null) {p1.delete();}
                    break;
                case 4:
                    p3 = new Picture(x, y, "Player/Idle/0" + counter + ".png");
                    p3.grow(10, 10);
                    p3.draw();
                    if (p2 != null) {p2.delete();}
                    break;
            }
            deleteRun();
            deleteLeftRun();

        }

    }
}


