package org.academiadecodigo.SSHewbaccas.Hapac.EnemyTypes;

import org.academiadecodigo.SSHewbaccas.Hapac.Player;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Slime {

    private Picture picture;
    private Player player;

    private int healthBar = 30;
    private int slimeCount;
    private int attackPower = 3;
    private boolean alive = true;
    private boolean flip = true;

    private int x;
    private int y;

    //slime attack left
    private Picture attLeft0;
    private Picture attLeft1;
    private Picture attLeft2;
    private Picture attLeft3;

    //slime attack right

    private Picture attRight0;
    private Picture attRight1;
    private Picture attRight2;
    private Picture attRight3;

    public Slime(int x, int y, int healthBar, Player player) {
        this.healthBar = healthBar;
        this.player = player;
        this.x = x;
        this.y = y;

        slimeSpawn();
    }

    public void setHealthBar(int damage) {
        healthBar -= damage;
    }

    public int getHealthBar() {
        return healthBar;
    }

    public int getX() {
        return x;
    }

    public void moveLeft(int counter) {

        if (player.getX() < x) {

            if (!flip) {
                picture.delete();
                picture = new Picture(x, y, "/Slime/Idle/slime-idle-0.png");
                picture.grow(3, 10);
                picture.draw();
                flip = true;
            }

            while ((player.getX() + 40) == x) {
                x -= 0;
                attackLeft();
                attack();

                attLeft0.delete();
                attLeft1.delete();
                attLeft2.delete();
                attLeft3.delete();
                picture.delete();
                if (player.getX() + 40 != x) {
                    picture.draw();
                }
            }

            x -= 4;
            picture.translate(-4, 0);
            /*System.out.println("slime pos: " + x);*/
        }
    }

    public void moveRight(int counter) {

        if (player.getX() > x) {

            if (flip) {
                picture.delete();
                picture = new Picture(x, y, "/Slime/Idle/slime-idle-flipped-0.png");
                picture.grow(3, 10);
                picture.draw();
                flip = false;
            }

            while (player.getX() == x + 40) {
                x += 0;
                attackRight();
                attack();
                attRight0.delete();
                attRight1.delete();
                attRight2.delete();
                attRight3.delete();
                picture.delete();

                if (player.getX() + 40 != x) {
                    picture.draw();
                }
            }

            x += 4;
            picture.translate(4, 0);
            /*System.out.println("slime pos: " + x);*/
        }
    }

    public void slimeSpawn() {
        attackPower++;
        slimeCount++;
        picture = new Picture(x, y, "Slime/Idle/slime-idle-0.png");
        picture.grow(3, 10);
        picture.draw();
    }

    public void slimeDelete(){
        picture.delete();
    }

    public void attack() {
        player.setPlayerHealthBar(attackPower);
    }

    public void attackLeft() {
        //Player is on the left

        attLeft0 = new Picture(x, y, "Slime/Attack/0.png");
        attLeft0.grow(3, 10);
        attLeft0.draw();

        attLeft1 = new Picture(x, y, "Slime/Attack/1.png");
        attLeft1.grow(3, 10);
        attLeft1.draw();

        attLeft2 = new Picture(x, y, "Slime/Attack/2.png");
        attLeft2.grow(3, 10);
        attLeft2.draw();

        attLeft3 = new Picture(x, y, "Slime/Attack/3.png");
        attLeft3.grow(3, 10);
        attLeft3.draw();

    }

    public void attackRight() {
        //Player is on the right
        attRight0 = new Picture(x, y, "Slime/Attack/0-flipped.png");
        attRight0.grow(3, 10);
        attRight0.draw();
        attRight1 = new Picture(x, y, "Slime/Attack/1-flipped.png");
        attRight1.grow(3, 10);
        attRight1.draw();
        attRight2 = new Picture(x, y, "Slime/Attack/2-flipped.png");
        attRight2.grow(3, 10);
        attRight2.draw();
        attRight3 = new Picture(x, y, "Slime/Attack/3-flipped.png");
        attRight3.grow(3, 10);
        attRight3.draw();
    }
}