package org.academiadecodigo.SSHewbaccas.Hapac;

import org.academiadecodigo.SSHewbaccas.Hapac.EnemyTypes.Slime;
import org.academiadecodigo.SSHewbaccas.Hapac.GridStuff.SimpleGfxGrid;

public class Game {

    private SimpleGfxGrid grid;

    private Player player;
    private Slime slime;

   // private int delay;
    private Movement movement;

    public Game() {
        /*grid = new SimpleGfxGrid();
        player = new Player(8, 695, grid);
        movement = new Movement(player);

        slime = new Slime(800 , 705, 30, player);
        grid.init();
         */
        this.init();

    }

    //Randomly spawns enemies in field. TODO: implement map spawn as well.
    public void init() {

        System.out.println("Game controls:" +
                "\n" + "To move, use A and D." +
                "\n" + "To commit slimecide, press K." +
                "\n" + "To exit this fuckfest, press Q." +
                "\n" + "To jump, use your imagination.");

        try {
            Thread.sleep(300);

            grid = new SimpleGfxGrid();
            player = new Player(8, 695, grid);
            movement = new Movement(player);
            movement.init();
            grid.init();
            slime = new Slime(800 , 705, 30, player);

            start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
       // slime = new Slime(800 , 705, 30, player);
    }

    int counter = 1;
    //Starts the animations.
    public void start() throws InterruptedException {

        while(true) {
            if (counter == 5) {
                counter = 1;
            }

            Thread.sleep(100);
            moveAllSprites();
            attackRange();
            counter++;

        }
    }
    //Has to get all other print methods, iterate over them, and after every sleep it will run over them.
    private void moveAllSprites() {
        player.getCounter(counter);
        player.idlePlayer(counter);
        slime.moveRight(counter);
        slime.moveLeft(counter);
    }

    public void attackRange(){
        if(player.getX() < slime.getX() && player.getX() + 20 > slime.getX() && player.playerAttackToRight){
            slime.setHealthBar(player.getPlayerDamage());
            /*System.out.println(slime.getHealthBar());*/

            player.playerAttackToRight = false;

            if(slime.getHealthBar() <= 0){
                slime.slimeDelete();
                slime = null;
                slime = new Slime(800, 705, 30, player);
            }
            /*slime.attack();*/
            /*System.out.println(player.getPlayerHealthBar());*/
        }

        if(player.getX() > slime.getX() && player.getX() < slime.getX() + 20 && player.playerAttackToLeft){
            slime.setHealthBar(player.getPlayerDamage());
            /*System.out.println(slime.getHealthBar());*/

            player.playerAttackToLeft = false;

            if(slime.getHealthBar() <= 0){
                slime.slimeDelete();
                slime = null;
                slime = new Slime(800, 705, 30, player);
            }
        }
    }
}
